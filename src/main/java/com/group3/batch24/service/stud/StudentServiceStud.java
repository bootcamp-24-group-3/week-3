package com.group3.batch24.service.stud;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.group3.batch24.model.Student;
import com.group3.batch24.repository.StudentRepository;
import com.group3.batch24.service.StudentService;

import com.group3.batch24.exception.ResourceNotFoundException;
import org.springframework.validation.ObjectError;


@Service
public class StudentServiceStud implements StudentService{

	private StudentRepository studentRepository;
	
	
	public StudentServiceStud(StudentRepository studentRepository) {
		super();
		this.studentRepository = studentRepository;
	}


	@Override
	public Student saveStudent(Student student) {
		// ito ay para sa postmapping save data to db
		return studentRepository.save(student);
		
	}


	@Override
	public List<Student> getAllStudents() {
		// TODO Auto-generated method stub
		return studentRepository.findAll();
	}

	@Override
	public List<Object[]> getAllStud() {
		return studentRepository.getSortedStudentsByNamesGroupedByYearLevelAndSection();
	}


	@Override
	public Student getStudentByID(long id) {
		Optional<Student>student= studentRepository.findById(id);
		return studentRepository.findById(id).orElseThrow(()
				-> new ResourceNotFoundException("Student", "Id", id));
		
	}


	@Override
	public Student updateStudent(Student student, long id) {
		Student existingStudent = studentRepository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Student","Id",id));
		
		existingStudent.setStudentName(student.getStudentName());
		existingStudent.setAddress(student.getAddress());
		existingStudent.setAge(student.getAge());
		existingStudent.setCity(student.getCity());
		existingStudent.setEmail(student.getEmail());
		existingStudent.setMob_num(student.getMob_num());
		existingStudent.setPhone_num(student.getPhone_num());
		existingStudent.setProvince(student.getProvince());
		existingStudent.setRegion(student.getRegion());
		existingStudent.setYr_lvl(student.getYr_lvl());
		existingStudent.setZip(student.getZip());
		
		
		

		
		//save existing employee to DB
		studentRepository.save(existingStudent);
		return existingStudent;

	}


	@Override
	public void deleteStudent(long id) {
		//check whether a employee exist in DB or not
				studentRepository.findById(id).orElseThrow(
						() -> new ResourceNotFoundException("Student","Id",id));
				
				studentRepository.deleteById(id);
		
	}



}
