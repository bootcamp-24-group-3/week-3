package com.group3.batch24.service;

import java.util.List;
import java.util.Optional;

import com.group3.batch24.model.Student;

public interface StudentService {

	//ito ay pag save ng data using PostMapping
		Student saveStudent(Student student);

		List<Student> getAllStudents();
	
		Student getStudentByID(long id);
		
		
		Student updateStudent (Student student, long id);
		
		void deleteStudent(long id);

		List<Object[]> getAllStud();

}
