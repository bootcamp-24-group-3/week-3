package com.group3.batch24.model;


import javax.persistence.Column;
//import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data 
@Entity
@Table(name="sample_student")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "student")
	private String studentName;
	
	@Column(name = "age")
	private int age;
	
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "zip")
	private int zip;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "region")
	private String region;
	
	@Column(name = "province")
	private String province;
	
	@Column(name = "phone_num")
	private int phone_num;
	
	@Column(name = "mob_num")
	private int mob_num;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "yr_lvl")
	private int yr_lvl;

	
	@Column(name = "section")
	private String section;
	
	public Student() {
		super();
	
	}


public Student(long id, String studentName, int age, String address, int zip, String city, String region,
		String province, int phone_num, int mob_num, String email, int yr_lvl) {
	super();
	this.id = id;
	this.studentName = studentName;
	this.age = age;
	this.address = address;
	this.zip = zip;
	this.city = city;
	this.region = region;
	this.province = province;
	this.phone_num = phone_num;
	this.mob_num = mob_num;
	this.email = email;
	this.yr_lvl = yr_lvl;
}


public long getId() {
	return id;
}


public void setId(long id) {
	this.id = id;
}


public String getStudentName() {
	return studentName;
}


public void setStudentName(String studentName) {
	this.studentName = studentName;
}


public int getAge() {
	return age;
}


public void setAge(int age) {
	this.age = age;
}


public String getAddress() {
	return address;
}


public void setAddress(String address) {
	this.address = address;
}


public int getZip() {
	return zip;
}


public void setZip(int zip) {
	this.zip = zip;
}


public String getCity() {
	return city;
}


public void setCity(String city) {
	this.city = city;
}


public String getRegion() {
	return region;
}


public void setRegion(String region) {
	this.region = region;
}


public String getProvince() {
	return province;
}


public void setProvince(String province) {
	this.province = province;
}


public int getPhone_num() {
	return phone_num;
}


public void setPhone_num(int phone_num) {
	this.phone_num = phone_num;
}


public int getMob_num() {
	return mob_num;
}


public void setMob_num(int mob_num) {
	this.mob_num = mob_num;
}


public String getEmail() {
	return email;
}


public void setEmail(String email) {
	this.email = email;
}


public int getYr_lvl() {
	return yr_lvl;
}


public void setYr_lvl(int yr_lvl) {
	this.yr_lvl = yr_lvl;
}

	@Override
	public String toString() {
		return "Student{" +
				"id=" + id +
				", studentName='" + studentName + '\'' +
				", age=" + age +
				", address='" + address + '\'' +
				", zip=" + zip +
				", city='" + city + '\'' +
				", region='" + region + '\'' +
				", province='" + province + '\'' +
				", phone_num=" + phone_num +
				", mob_num=" + mob_num +
				", email='" + email + '\'' +
				", yr_lvl=" + yr_lvl +
				'}';
	}
}
