package com.group3.batch24.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.group3.batch24.model.Student;

import java.util.List;
import java.util.Optional;


@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("SELECT student.id, student.section, student.yr_lvl FROM Student student ORDER BY student.studentName ASC")
    List<Object[]> getSortedStudentsByNamesGroupedByYearLevelAndSection();

}
