package com.group3.batch24.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.group3.batch24.model.Student;
import com.group3.batch24.service.StudentService;




@RestController
@RequestMapping("/api/student")
public class StudentController {

	@Autowired
	private StudentService studentService;

	public StudentController(StudentService studentService) {
		super();
		this.studentService = studentService;
	}
	
	//build create employee REST API 
	@PostMapping()
	public ResponseEntity<Student> saveStudent(@RequestBody Student student){
		return new ResponseEntity<Student>(studentService.saveStudent(student),HttpStatus.CREATED);
	}

	@GetMapping()	
	public List<Student> getAllStudents(){
		return studentService.getAllStudents();
	}

	@GetMapping("sortByNamesB")
	public List<Object[]> getAll(){
		return studentService.getAllStud();
	}
	
	@GetMapping("{id}") // {id} is a syntax to get path variable
	public ResponseEntity<Student> getStudentById(@PathVariable("id")long studentId){
		return new ResponseEntity<Student>(studentService.getStudentByID(studentId),HttpStatus.OK);
	}
	
	
	@PutMapping("{id}")
	public ResponseEntity<Student> updateStudent(@PathVariable("id") long id
												  ,@RequestBody Student student){
		return new ResponseEntity<Student>(studentService.updateStudent(student, id), HttpStatus.OK);
		
	}
	

	@DeleteMapping("{id}")
	public ResponseEntity<String> deleteStudent(@PathVariable("id") long id){
			 //delete student from db
		
		studentService.deleteStudent(id);
		
		return new ResponseEntity<String>("Student deleted Successfully",HttpStatus.OK);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
